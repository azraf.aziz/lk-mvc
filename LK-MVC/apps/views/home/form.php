<form method="POST">
  <div class="row mb-3">

    <?php if (isset($data['id'])) : ?>
    <input type="hidden" name="id" value="<?= isset($data['id'])? $data['id'] : "" ?>"/>
    <?php endif ?>
    
    <label for="inputNama" class="col-sm-2 col-form-label">Nama Barang</label>
    <div class="col-sm-10">
      <input name="nama" type="text" class="form-control" id="inputNama" value="<?= isset($data['nama'])? $data['nama'] : "" ?>">
    </div>
  </div>
  <div class="row mb-3">
    <label for="inputJumlah" class="col-sm-2 col-form-label">Jumlah</label>
    <div class="col-sm-10">
      <input name="qty" type="text" class="form-control" id="inputJumlah" value="<?= isset($data['qty'])? $data['qty'] : "" ?>">
    </div>
  </div>
  <button type="submit" class="btn btn-primary">Simpan</button>
</form> 
